package cloudcode.controllers;

import cloudcode.models.Cart;
import cloudcode.services.CartService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/carts")
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping(value = "/")
    public List<Cart> getAllCarts() {
        return cartService.findAll();
    }

    @GetMapping(value = "/{cartId}")
    public Cart getCart(@PathVariable("cartId")ObjectId cartId) {
        return cartService.findOneById(cartId);
    }
}
