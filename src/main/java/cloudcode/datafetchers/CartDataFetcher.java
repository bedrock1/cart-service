package cloudcode.datafetchers;

import cloudcode.models.Cart;
import cloudcode.services.CartService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CartDataFetcher implements DataFetcher<List<Cart>> {

    private final CartService cartService;

    @Autowired
    CartDataFetcher(CartService cartService) {
        this.cartService = cartService;
    }

    @Override
    public List<Cart> get(DataFetchingEnvironment environment) {
        List<Cart> carts = cartService.findAll();
        return carts;
    }
}
