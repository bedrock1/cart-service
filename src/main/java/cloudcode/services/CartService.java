package cloudcode.services;

import cloudcode.models.Cart;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.List;

@Component
public interface CartService {

    List<Cart> findAll();
    Cart findOneById(ObjectId id);
    List<Cart> findByIdIn(List<ObjectId> ids);
}
