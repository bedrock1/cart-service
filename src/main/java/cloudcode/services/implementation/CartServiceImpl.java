package cloudcode.services.implementation;

import cloudcode.models.Cart;
import cloudcode.repositories.CartRepository;
import cloudcode.services.CartService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CartServiceImpl implements CartService {

    private final  CartRepository cartRepository;

    @Autowired
    CartServiceImpl(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public List<Cart> findAll() {
        return (ArrayList) cartRepository.findAll();
    }

    @Override
    public Cart findOneById(ObjectId id) {
        return cartRepository.findOne(id);
    }

    @Override
    public List<Cart> findByIdIn(List<ObjectId> ids) {
        return cartRepository.findByIdIn(ids);
    }
}
