package cloudcode.repositories;
import cloudcode.models.Cart;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CartRepository extends PagingAndSortingRepository<Cart, ObjectId> {

    List<Cart> findByIdIn(List<ObjectId> ids);
}
